﻿namespace CoAHomework
{
    public class EnemyBullet : Enemy
    {
        public int      bulletCount;
        public int      bulletShards;

        public float    bulletSpeed;
        public float    bulletDamage;

        public string   bulletOwner;
        public string   bulletName;
        public string   bulletTarget;

        public bool     isSharp;
        public bool     doesDamage;
        public bool     isFlying;
    }
}