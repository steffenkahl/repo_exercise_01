﻿namespace CoAHomework
{
    public class EnemyDesign : Enemy
    {
        public int      designID;
        public int      hatSize;

        public float    hatXPosition;
        public float    hatYPosition;

        public string   designName;
        public string   artistName;
        public string   copyrightText;

        public bool     isAvailable;
        public bool     isBought;
        public bool     isDeleted;
        public bool     isEnchanted;
    }
}