﻿namespace CoAHomework
{
    public class EnemyBehaviour : Enemy
    {
        public int      bulletCount;
        public int      lives;

        public float    speed;
        public float    jumpHeight;

        public string   name;
        public string   id;
        public string   makerName;

        public bool     isRunning;
        public bool     isReloading;
        public bool     isDumb;
        public bool     isSneaking;
    }
}