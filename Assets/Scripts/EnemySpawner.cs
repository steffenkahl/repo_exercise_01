﻿namespace CoAHomework
{
    public class EnemySpawner : Enemy
    {
        public int      spawnCounter;
        public int      spawnAtSameTime;

        public float    spawnTimer;
        public float    spawnPropability;

        public string   spawnPositionName;
        public string   spawnedMobName;
        public string   spawnedMobCreator;

        public bool     isSpawning;
        public bool     isFull;
        public bool     isOverloaded;
        public bool     isOutside;
    }
}