﻿namespace CoAHomework
{
    public class EnemyPainter : Enemy
    {
        public int      brushID;
        public int      layer;

        public float    brushSize;
        public float    brushHardness;

        public string   toolName;
        public string   projectName;
        public string   creatorName;

        public bool     isDrawing;
        public bool     isErasing;
        public bool     isRotating;
        public bool     isRainbow;
    }
}